<?php

namespace App\Http\Controllers;

use App\Http\Requests\NearBySearchRequest;
use Exception;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class MapController extends Controller
{
    public function nearBySearch(NearBySearchRequest $request)
    {

        $latitude = $request->lat;
        $longitude = $request->long;

        try{
            $client = new Client();

            $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';

            $params = [
                'query' => [
                    'location' => $latitude.','.$longitude,
                    'radius' => '2000',
                    'key' => env('GOOGLE_MAP_API_KEY'),
                ],
            ];

            $response = $client->get($url, $params);

            $places = json_decode($response->getBody(), true);

            $placeList = [];
            foreach($places['results'] as $place)
            {
                $tempPlace['name'] = $place['name'];
                $tempPlace['address'] = $place['vicinity'];
                $tempPlace['geometry_details'] = $place['geometry']['location'];
                $tempPlace['avg_rating'] = $place['rating']??0;
                $tempPlace['total_rating'] = $place['user_ratings_total']??0;
                array_push($placeList, $tempPlace);
            }

            return response()->json($placeList, 200);
        }catch(Exception $err)
        {
            return response()->json([
                'success'=>false,
                'message'=>$err->getMessage()
            ], 500);
        }
    }
}
