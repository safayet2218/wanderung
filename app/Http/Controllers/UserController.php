<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function signUp(SignUpRequest $request)
    {
        try{
            $user = new User;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $response = ['status'=>true, 'message'=>"Registration is done successfully!"];

            return response()->json($response, 201);
        }catch(Exception $err)
        {
            $response = ['status'=>false, 'message'=>$err->getMessage()];
            return response()->json($response, 500);
        }
    }

    public function signIn(SignInRequest $request)
    {
        try{
            if(auth()->attempt(['email' => $request->email, 'password'=> $request->password]))
            {
                $user = User::where('email',$request->email)->first();
                
                $access_token = Auth::user()->createToken('Token')->accessToken;
                
                $data = [
                    'token' => $access_token,
                    'user' => $user
                ];

                return response()->json([
                    'success'=>true, 
                    'message'=> 'Login Successfully', 
                    'data'=>$data
                ], 200);
            }else{
                return response()->json([
                    'success'=>false, 
                    'message'=> 'Invalid Credentials'
                ], 401);
                
            }
        }catch(Exception $err)
        {
            $response = ['status'=>false, 'message'=>$err->getMessage()];
            return response()->json($response, 500);
        }
        
    }
}
