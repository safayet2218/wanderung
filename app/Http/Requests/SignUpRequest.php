<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'email'=>'required|email|unique:users,email',
            'password'=>'required|string|min:8',
            'confirm_password'=>'required|string|min:8'
        ];

        if(isset($this->confirm_password))
        {
            $rules['password'] = 'same:confirm_password';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'email.required' => 'Please, enter your email!',
            'email.email' => 'Invalid email format!',
            'email.unique' => 'This email has already been taken!',
            'password.required' => 'Please, enter password!',
            'password.same' => 'Password and Confirm password is not same!',
            'password.string'=>'Password must be string!',
            'confirm_password.required' => 'Please, enter confirm password!',
            'confirm_password.string'=>'Confirm password must be string!',

        ];

        return $messages;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = [
                'success'=>false,
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 422);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
