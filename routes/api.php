<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
use App\Http\Controllers\UserController;
use App\Http\Controllers\MapController;

Route::post('signup', [UserController::class, 'signUp']);
Route::post('login', [UserController::class, 'signIn']);

Route::get('places', [MapController::class, 'nearBySearch'])->middleware('auth:api');

